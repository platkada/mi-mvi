import style from './style';

export default function Button() {
	const {children, ...props} = this.props;
	return (
		<button class={style.button} {...props}>{children}</button>
	);
}
