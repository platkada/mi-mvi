import { Component, createRef } from 'preact';
import style from './style';
import Breeder from '../../lib/Breeder';
import Player from '../../lib/Player';
import { multirange } from '../../lib/util';
import Button from '../button';

class TooltipDetail extends Component {
	state = {
		active: false
	};

	toggle = () => {
		this.setState({
			active: !this.state.active
		});
	};

	renderTooltip() {
		return (
			<div class={style.tooltip}>
				{this.props.children}
			</div>
		);
	}

	render() {
		return (
			<div style="display: inline-block; position:relative;">
				<Button onClick={this.toggle}>
					{this.props.title}
				</Button>
				{this.state.active && this.renderTooltip()}
			</div>
		);
	}
}

class LayerWeightViewer extends Component {
	canvas = createRef();
	noteWidth = 10;
	noteHeight = 4;

	draw = evt => {
		const { cppn } = this.props;
		const { noteRangeMin } = Breeder.settings;
		const canvas = this.canvas.current;
		const ctx = canvas.getContext('2d');
		const rect = evt.target.getBoundingClientRect();
		const x2 = evt.clientX - rect.left;
		const y2 = evt.clientY - rect.top;

		const fn = Breeder.weightFunction(cppn);
		const imageData = ctx.createImageData(canvas.width, canvas.height);

		for (let y1 = 0; y1 < canvas.height; y1++) for (let x1 = 0; x1 < canvas.width; x1++) {
			const v = Breeder.evaluateWeightFunction(fn, x1, y1 - noteRangeMin, x2, y2 - noteRangeMin) * 128 + 128;
			imageData.data[(canvas.width * y1 + x1) * 4 + 0] = v;
			imageData.data[(canvas.width * y1 + x1) * 4 + 1] = v;
			imageData.data[(canvas.width * y1 + x1) * 4 + 2] = v;
			imageData.data[(canvas.width * y1 + x1) * 4 + 3] = 255;
		}

		ctx.putImageData(imageData, 0, 0);
	};

	save = () => {
		const link = document.createElement('a');
		link.href = this.canvas.current.toDataURL();
		link.download = `weights_${this.props.title.replace(/\W+/g, '_')}.png`;
		link.click();
	};

	render() {
		const { noteRangeMin, noteRangeMax, barLength } = Breeder.settings;
		const width = barLength;
		const height = (noteRangeMax - noteRangeMin + 1);

		return (
			<div style="border: 1px solid #ccc; margin-right: -1px;">
				<div style="font-size: 0.5rem; font-weight: bold;">{this.props.title}</div>
				<canvas ref={this.canvas} onMouseMove={this.draw} onClick={this.save} width={width} height={height} style={{ width: width * this.noteWidth + 'px', height: height * this.noteHeight + 'px' }} />
			</div>
		);
	}
}

function WeightViewer(props) {
	function makeTitleComponent(i) {
		if (i === 0) return 'in';
		if (i === Breeder.settings.hiddenLayers + 1) return 'out';
		return i;
	}

	function makeTitle(i) {
		return `${makeTitleComponent(i)} -> ${makeTitleComponent(i + 1)}`;
	}

	return (
		<div style="display: flex;">
			{props.individual.cppns.map((n, i) => <LayerWeightViewer title={makeTitle(i)} cppn={n} />)}
		</div>
	);
}

class ScorePreview extends Component {
	canvas = createRef();
	noteWidth = 5;
	noteHeight = 2;
	padding = 1;

	draw() {
		const score = Breeder.score(this.props.individual);

		const canvas = this.canvas.current;
		const ctx = canvas.getContext('2d');

		ctx.clearRect(0, 0, canvas.width, canvas.height);

		ctx.beginPath();

		for (const [note, time] of multirange([score.noteMin, score.noteMax + 1], score.length())) {
			if (score.get(note, time)) {
				const x = time * (this.noteWidth + this.padding);
				const y = (score.noteMax - note) * (this.noteHeight + this.padding);
				ctx.rect(x, y, this.noteWidth, this.noteHeight);
			}
		}

		ctx.fill();
	}

	componentDidMount() {
		this.draw();
	}

	componentDidUpdate(previousProps) {
		if (previousProps.individual !== this.props.individual) this.draw();
	}

	render() {
		const { individual, ...props } = this.props;
		const score = Breeder.score(individual);
		const width = score.length() * (this.noteWidth + this.padding);
		const height = (score.noteMax - score.noteMin + 1) * (this.noteHeight + this.padding);
		return (
			<canvas ref={this.canvas} width={width} height={height} {...props} />
		);
	}
}

class Individual extends Component {
	handlePlayClick = () => {
		Player.play(Breeder.score(this.props.individual));
	};

	handleMIDIClick = () => {
		const score = Breeder.score(this.props.individual);
		const uri = score.midi();
		window.open(uri);
	};

	render() {
		return (
			<div class={[style.individual, this.props.selected ? style.selected : ''].join(' ')}>
				<div class={style.buttons}>
					<Button onClick={this.handlePlayClick}>play</Button>
					<Button onClick={this.handleMIDIClick}>MIDI</Button>
					<TooltipDetail title="weights"><WeightViewer individual={this.props.individual} /></TooltipDetail>
				</div>
				<ScorePreview individual={this.props.individual} onClick={this.props.onClick} />
			</div>
		);
	}
}

export default class Picker extends Component {
	state = {
		population: Breeder.population,
		selection: []
	};

	handleIndividualClick(individual) {
		const selection = this.state.selection;

		let nextSelection;

		if (this.isSelected(individual)) nextSelection = selection.filter(i => i !== individual);
		else nextSelection = [...selection, individual];

		this.setState({
			selection: nextSelection
		});
	}

	isSelected(individual) {
		return this.state.selection.includes(individual);
	}

	handleNextClick = () => {
		Player.stop();
		Breeder.next(this.state.selection);

		this.setState({
			population: Breeder.population,
			selection: []
		});
	};

	handleResetClick = () => {
		Breeder.restart();

		this.setState({
			population: Breeder.population,
			selection: []
		});
	};

	renderIndividual(individual) {
		return <Individual individual={individual} selected={this.isSelected(individual)} onClick={() => this.handleIndividualClick(individual)} />;
	}

	render() {
		return (
			<>
				<Button onClick={this.handleResetClick}>Reset</Button>
				<div class={style.picker}>
					{this.state.population.map(i => this.renderIndividual(i))}
				</div>
				<Button disabled={this.state.selection.length === 0} onClick={this.handleNextClick}>Next</Button>
			</>
		);
	}
}
