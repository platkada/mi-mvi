import Tone from 'tone';
import { range } from './util';

class Player {
	part = null;
	synth = new Tone.PolySynth(8, Tone.Synth).toMaster();

	setBPM(bpm) {
		Tone.Transport.bpm.rampTo(bpm, 1);
	}

	play(score) {
		this.stop();

		const noteMin = score.noteMin;
		const noteMax = score.noteMax;

		if (this.part) this.part.dispose();

		const events = [...range(score.length())].map(t => {
			const notes = [];

			for (let note = noteMin; note <= noteMax; note++) {
				if (score.get(note, t)) notes.push(this.convertNote(note));
			}

			return [this.convertTime(t), notes];
		});

		this.part = new Tone.Part((time, notes) => {
			this.synth.triggerAttackRelease(notes, '16n', time);
		}, events);

		this.part.start(0);
		Tone.Transport.start();
	}

	stop() {
		Tone.Transport.stop();
	}

	convertTime(time) {
		// TODO: generalize

		time *= 2;

		const sixteenths = time % 4;
		const quarters = ((time - sixteenths) / 4) % 4;
		const bars = Math.floor(time / 16);

		return `${bars}:${quarters}:${sixteenths}`;
	}

	convertNote(note) {
		return Math.pow(2., (note - 69.) / 12.) * 440.;
	}
}

export default new Player();
