import MidiWriter from 'midi-writer-js';
import { lerp, memoize, randomElement, range } from './util';

//=============================================================================
// SCORE
//=============================================================================

class Score {
	constructor(noteMin, noteMax, barLength, bars) {
		this.noteMin = noteMin;
		this.noteMax = noteMax;
		this.barLength = barLength;
		this.bars = bars;
	}

	length() {
		return this.bars.length * this.barLength;
	}

	get(note, time) {
		if (note < this.noteMin || note > this.noteMax || time < 0 || time > this.length()) return 0;

		const bar = this.bars[Math.floor(time / this.barLength)];
		return bar[(note - this.noteMin) * this.barLength + (time % this.barLength)] > 0.5;
	}

	midi() {
		const pitchesAt = time => {
			const pitches = [];
			for (let note = this.noteMin; note <= this.noteMax; note++) {
				if (this.get(note, time)) pitches.push(note);
			}
			return pitches;
		};

		const track = new  MidiWriter.Track();
		let pitches = pitchesAt(0);
		const length = this.length();

		for (let time = 0; time < length; time++) {
			let nextPitches;
			let wait = 0;
			const waitArray = [];
			while ((time + 1 + wait < length) && !(nextPitches = pitchesAt(time + 1 + wait)).length) {
				wait++;
				waitArray.push('16');
			}
			const event = new MidiWriter.NoteEvent({ pitch: pitches, duration: '16', wait: waitArray });
			track.addEvent(event);
			pitches = nextPitches;
			time += wait;
		}

		const writer = new MidiWriter.Writer(track);
		return writer.dataUri();
	}
}

//=============================================================================
// GENE
//=============================================================================

let nextInnovationNum = 1;

export const ActivationFunctions = {
	sigmoid: v => 2.0 / (1.0 + Math.exp(-4.9 * v)) - 1.0,
	fract: v => (v -= 0.5, v - Math.floor(v))
	// gauss: v => Math.exp(-10 * v * v)
	// gauss: v => Math.exp(-4.5 * v * v)
};

const activationFunctions = Object.keys(ActivationFunctions);

class Gene {
	constructor(from, to, weight, fn, innovationNum = nextInnovationNum++, enabled = true) {
		this.from = from;
		this.to = to;
		this.weight = weight;
		this.fn = fn;
		this.innovationNum = innovationNum;
		this.enabled = enabled;
	}

	clone() {
		return new Gene(this.from, this.to, this.weight, this.fn, this.innovationNum, this.enabled);
	}
}

//=============================================================================
// CPPN
//=============================================================================

class CPPN {
	constructor(genome, parents = []) {
		this.genome = genome;
		this.nodeCount = parents.reduce((max, p) => Math.max(max, p.nodeCount), 1);
		// this.parents = parents;
	}

	clone() {
		return new CPPN(this.genome.map(g => g.clone()), [this]);
	}
}

//=============================================================================
// INDIVIDUAL
//=============================================================================

let nextIndividualID = 1;

class Individual {
	constructor(biases, cppns, parents) {
		this.id = nextIndividualID++;
		this.biases = biases;
		this.cppns = cppns;
		// this.parents = parents;
	}

	clone() {
		return new Individual(new Float32Array(this.biases), this.cppns.map(n => n.clone()), [this]);
	}
}

//=============================================================================
// BREEDER
//=============================================================================

function defaultSettings() {
	return {
		populationSize: 15,
		hiddenLayers: 2,
		randomWeightMax: 3,
		perturbationProbability: 0.8,
		perturbationMax: 0.1,
		randomBiasMax: 1e-7,
		biasPerturbationMax: 1e-7,
		connectionProbability: 0.5,
		additionProbability: 0.5,
		noteOffsetMax: 12,
		barLength: 8,
		noteRangeMin: 36, // C2
		noteRangeMax: 60, // C4
		defaultScoreBars: 4
	};
}

const Breeder = {
	settings: defaultSettings(),

	inputs: ['x1', 'y1', 'x2', 'y2', 'bias'],

	restart() {
		this.population = this.initialPopulation();
	},

	initialPopulation() {
		const population = [];
		while (population.length < this.settings.populationSize) {
			const cppns = [];
			while (cppns.length <= this.settings.hiddenLayers) cppns.push(new CPPN(this.initialGenome()));
			population.push(new Individual(this.initialBiases(), cppns, []));
		}
		return population;
	},

	initialBiases() {
		const { barLength, noteRangeMin, noteRangeMax, randomBiasMax } = this.settings;
		const count = barLength * (noteRangeMax - noteRangeMin + 1);
		const biases = new Float32Array(count);
		for (let i = 0; i < count; i++) biases[i] = lerp(-randomBiasMax, randomBiasMax, Math.random());
		return biases;
	},

	initialGenome() {
		return this.inputs.map(i => new Gene(i, 0, this.randomWeight(), randomElement(activationFunctions)));
	},

	randomWeight() {
		return lerp(-this.settings.randomWeightMax, this.settings.randomWeightMax, Math.random());
	},

	@memoize
	weightFunction(cppn) {
		let body = `const values = new Float32Array(${cppn.nodeCount});\n`;

		const sorted = this.genomeTopSort(cppn.genome);

		for (const gene of sorted) {
			if (!gene.enabled) continue;
			const valueStr = this.inputs.includes(gene.from) ? gene.from : `values[${gene.from}]`;
			const fnStr = valueStr === 'bias' ? 'bias' : `fns.${gene.fn}(${valueStr})`;
			body += `values[${gene.to}] += ${gene.weight} * ${fnStr};\n`;
		}

		body += 'return fns.sigmoid(values[0]) * 2 - 1;';

		// eslint-disable-next-line no-new-func
		return new Function(...this.inputs, 'fns', body);
	},

	genomeTopSort(genome) {
		const res = [];
		const visited = new WeakSet();

		const visit = gene => {
			if (!gene.enabled || !gene.weight || visited.has(gene)) return;

			visited.add(gene);

			// TODO: precompute predecessors
			if (!this.inputs.includes(gene.from)) {
				for (const g of genome) {
					if (g.to === gene.from) visit(g);
				}
			}

			res.push(gene);
		};

		// visit output
		for (const gene of genome) {
			if (gene.to === 0 && gene.enabled && gene.weight) visit(gene);
		}

		return res;
	},

	crossover(parent1, parent2) {
		const biases = this.crossoverBiases(parent1.biases, parent2.biases);
		const cppns = [];
		for (let i = 0; i <= this.settings.hiddenLayers; i++) {
			cppns.push(this.crossoverCPPN(parent1.cppns[i], parent2.cppns[i]));
		}
		return new Individual(biases, cppns, [parent1, parent2]);
	},

	crossoverBiases(parent1, parent2) {
		// TODO
		return parent1;
	},

	crossoverCPPN(parent1, parent2) {
		const offspring = new CPPN([], [parent1, parent2]);

		let i1 = 0;
		let i2 = 0;
		const max = Math.min(parent1.genome.length, parent2.genome.length);

		// exploit monotony of innovationNum
		while (i1 < max && i2 < max) {
			const g1 = parent1.genome[i1];
			const g2 = parent1.genome[i2];

			if (g1.innovationNum < g2.innovationNum) {
				// include non-matching gene from parent1

				offspring.genome.push(g1.clone());
				i1++;
			}
			else if (i1 > i2) {
				// exclude non-matching gene from parent2

				i2++;
			}
			else {
				// randomly select matching gene from one parent

				// if g1 is disabled, clone it
				// this is to propagate the "insert node" mutation
				// (potentially non-matching replacement genes are taken from g1)
				const gene = g1.enabled ? randomElement([g1, g2]).clone() : g1.clone();

				offspring.genome.push(gene);
				i1++;
				i2++;
			}
		}

		// add parent1's excess genes
		if (i1 < parent1.genome.length) {
			offspring.genome = offspring.genome.concat(parent1.genome.slice(i1));
		}

		return offspring;
	},

	mutate(individual) {
		this.mutateBiases(individual.biases);
		for (const cppn of individual.cppns) this.mutateCPPN(cppn);
	},

	mutateBiases(individual) {
		const { biasPerturbationMax } = this.settings;
		for (let i = 0; i < individual.length; i++) {
			individual[i] += lerp(-biasPerturbationMax, biasPerturbationMax, Math.random());
		}
	},

	mutateCPPN(individual) {
		for (const gene of individual.genome) {
			if (!gene.enabled) continue;
			if (Math.random() < this.settings.perturbationProbability) {
				gene.weight += lerp(-this.settings.perturbationMax, this.settings.perturbationMax, Math.random());
			}
		}

		const order = this.genomeTopSort(individual.genome).map(g => g.to);

		// TODO: track innovations

		if (Math.random() < this.settings.connectionProbability) {
			let node1 = randomElement([...this.inputs, ...range(individual.nodeCount)]);
			const nodes = [...(this.inputs.includes(node1) ? [] : this.inputs), ...range(individual.nodeCount)];
			const i1 = nodes.indexOf(node1);
			if (i1 >= 0) nodes.splice(i1, 1);
			let node2 = randomElement(nodes);

			// check direction
			if (order.lastIndexOf(node1) > order.lastIndexOf(node2)) [node1, node2] = [node2, node1];

			// TODO: check not already connected

			individual.genome.push(new Gene(node1, node2, this.randomWeight(), randomElement(activationFunctions)));
		}

		if (Math.random() < this.settings.additionProbability) {
			const node = individual.nodeCount++;

			const gene = randomElement(individual.genome);

			gene.enabled = false;

			individual.genome.push(new Gene(gene.from, node, 1, randomElement(activationFunctions)));
			individual.genome.push(new Gene(node, gene.to, gene.weight, gene.fn));
		}
	},

	produceOffspring(selected) {
		const parent1 = randomElement(selected);
		const parent2 = randomElement(selected);

		let offspring = (parent1 === parent2) ? parent1.clone() : this.crossover(parent1, parent2);

		this.mutate(offspring);

		return offspring;
	},

	next(selected) {
		this.population = [...selected];
		while (this.population.length < this.settings.populationSize) {
			this.population.push(this.produceOffspring(selected));
		}
	},

	@memoize
	score(individual) {
		const { barLength, noteRangeMin, noteRangeMax, defaultScoreBars, hiddenLayers } = this.settings;
		const noteRange = noteRangeMax - noteRangeMin + 1;
		const count = barLength * noteRange;

		const bars = [];
		let lastBar = new Float32Array(count); // -1st bar is empty

		for (let bar = 0; bar < defaultScoreBars; bar++) {
			let src = new Float32Array(count);

			for (let i = 0; i < count; i++) src[i] = (!bar) * individual.biases[i] + lastBar[i];

			for (let i = 0; i <= hiddenLayers; i++) {
				const cppn = individual.cppns[i];
				const fn = this.weightFunction(cppn);
				const dst = new Float32Array(count);

				for (let y1 = 0; y1 < noteRange; y1++) for (let x1 = 0; x1 < barLength; x1++) {
					for (let y2 = 0; y2 < noteRange; y2++) for (let x2 = 0; x2 < barLength; x2++) {
						const weight = this.evaluateWeightFunction(fn, x1, y1, x2, y2);
						dst[barLength * y1 + x1] += weight * src[barLength * y2 + x2];
					}
					dst[barLength * y1 + x1] = ActivationFunctions.sigmoid(dst[barLength * y1 + x1]);
				}

				src = dst;
			}

			lastBar = src;
			bars.push(lastBar);
		}

		return new Score(noteRangeMin, noteRangeMax, barLength, bars);
	},

	evaluateWeightFunction(fn, x1, y1, x2, y2) {
		const { barLength, noteRangeMin, noteRangeMax } = this.settings;
		const noteRange = noteRangeMax - noteRangeMin + 1;
		const rangeMid = Math.floor(noteRange / 2);
		return fn(x1 / barLength, (y1 - rangeMid) / 12, x2 / barLength, (y2 - rangeMid) / 12, 1, ActivationFunctions);
	}
};

Breeder.restart();

export default Breeder;
