export function* range(min, max) {
	if (arguments.length === 1) {
		max = min;
		min = 0;
	}

	for (let i = min; i < max; i++) yield i;
}

export function* multirange(...args) {
	const minMax = [args.pop()].flat();

	if (args.length === 0) {
		for (const i of range(...minMax)) yield [i];
	}
	else {
		for (const i of range(...minMax)) {
			for (const rest of multirange(...args)) yield [...rest, i];
		}
	}
}

export function memoize(target, name, descriptor) {
	const fn = descriptor.value;
	const cache = new WeakMap();
	descriptor.value = function(arg) {
		if (cache.has(arg)) return cache.get(arg);
		const res = fn.call(this, arg);
		cache.set(arg, res);
		return res;
	};
	return descriptor;
}

export function lerp(a, b, v) {
	return a + (b - a) * v;
}

export function randomInt(max) {
	return Math.floor(Math.random() * max);
}

export function randomElement(arr) {
	return arr[randomInt(arr.length)];
}
