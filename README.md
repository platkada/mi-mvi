# tunebreeder

Semestrálka na předmět MI-MVI.
Jde o webovou aplikaci pro uživatelem asistované šlechtění krátkých melodií na základě algoritmu HyperNEAT.
Inspirováno projektem picbreeder.

Krátkodobě je přístupná na https://r-flash.eu/tunebreeder

## Zadání

Vycházel jsem ze zadání

> Natrénujte generativní Rekurentní Neuronovou Síť na malých úsecích hudby (cca do 5 s) ve formátu MIDI. Vytvořte aplikaci (web), která podobně jako Picbreeder (či neurogram) umožní uživateli být fitness funkcí, nikoli však pro obrázky, ale právě pro hudbu. Uživatel tak zasahuje do průběhu evoluce a říká, co se mu líbí a přibližuje se tak kýženému výsledku.

Na základě konzultací bylo zadání specifikováno na použití [HyperNEAT](http://eplex.cs.ucf.edu/hyperNEATpage/). Podrobnější informace jsou k přečtení v reportu v tomto repozitáři.  

## Lokální spuštění

Máte-li nainstalované programy `node` a `npm`, sestává spuštění lokálního serveru z prvotního
```
npm install
```
a následného
```
npm run start
```

Na dotyčném stroji se tak aplikace zpřístupní na adrese http://0.0.0.0:8080.

Více detailů v odstavci **CLI Commands**.

## Zdroják

Veškerá podstatná část implementace je k nalezení v souboru `src/lib/Breeder.js`.
Vše ostatní se týká GUI a přehrávání hudby. 

## CLI Commands

Tento odstavec je ponechán z defaultního README vygenerovaného pomocí `preact-cli`.

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# test the production build locally
npm run serve

# run tests with jest and preact-render-spy 
npm run test
```

For detailed explanation on how things work, checkout the [CLI Readme](https://github.com/developit/preact-cli/blob/master/README.md).
